### Gabriel de Queiroz Cordeiro

### Sobre mim

Iniciei na área da tecnologia de informação durante um curso de eletrônica do ensino médio, no qual eu cursei uma matéria de programação. Ao término do curso, essa matéria era a que mais me estimulava. Fiz cinco semestres de Análise e Desenvolvimento de Sistemas pela UTFPR e pedi transferência para a Fatec SJC. Trabalho na área há alguns anos, hoje dividindo o tempo com a graduação em Tecnologia em Banco de Dados.

# Meus Projetos

### Em 2019-2 - Web bot para consulta de preços - **[(Gitlab do projeto)](https://gitlab.com/bielqcsjc/pi-webbot)**
![alt text](images/api1.jpg)
O primerio projeto teve a proposta definida pela própria instituição, a qual solicitou o desenvolvimento de um web bot para solucionar um problema de escolha do grupo. O problema escolhido pelo grupo foi a dificuldade em comparar preços de produtos em diversos e-commerces, então, a solução escolhida foi o desenvolvimento de um webbot, chamado Sherlockcups, que acessa diversos sites de venda online, encontra o melhor preço para o produto desejado e o armazena, criando um histórico de preços para o mesmo.

#### Tecnologias Utilizadas
* Python 3.8;
* Selenium - Ferramenta de automação de browser;
* MongoDB - Banco de Dados NoSql;
* Principais Bibliotecas Python:
    * **pymongo** - interação com nosso Banco de Dados;
    * **Selenium webdriver** - navegação pela Web através do WebDriver do Google Chrome;
    
#### Contribuições Pessoais
No projeto, contribuí para o desenvolvimento da raspagem de dados executada pelo web bot, pelo armazenamento dos dados em um banco de dados NoSql (MongoDB) e pela exibição dos resultados da busca e dos dados recuperados do banco de dados.
A raspagem foi feita usando o Selenium, uma ferramenta de automatização de browsers, usada normalmente para testar aplicações web, mas que na situação foi utilizada para acessar as plataformas de venda online, realizar a busca pelo produto e extrair o preço do mesmo na página. Contando com configurações para acessar as plataformas simulando um usuário real, prevenindo anti-bots, além de ajustes de CORS, o Selenium foi a melhor opção que pudemos definir.
A escolha pelo MongoDB se deu pela eficiência e pela estrutura dos dados armazenados, nem sempre teriam os mesmo atributos, então ter um SGBD capaz de lidar com essas situações foi de grande utilidade para o projeto, ainda mais com a utilização do Atlas, o servidor em nuvem do MongoDB.

#### Hard Skills Efetivamente Desenvolvidas
* Python: Utilização da linguagem para o desenvolvimento do projeto.
* Selenium: Utilizada para fazer a raspagem de dados, mas o aprendizado se estende para seu propósito real.
* MongoDB: Utilização do SGBD para crud dos dados, utilizando queries com funções da ferramenta do para obtenção dos resultados de forma mais eficiente.
* Metodologias Scrum: O projeto foi realizado seguindo o processo do SCRUM.

#### Soft Skills Efetivamente Desenvolvidas
* Trabalho em equipe: Início de um processo que se estendeu pelos demais projetos, onde trabalhar em grupo foi essencial para a execução do projeto.
* Organização de equipes: Distribuir as atividades entre os membros de equipe de forma que cada um tivesse seus pontos fortes utilizados da melhor forma possível
* Autodidatismo: Desenvolver a capacidade de buscar por novos conhecimentos para se desenvolver como profissional.

### Em 2020-1 - Gantt organizacional **[(Gitlab do projeto)](https://gitlab.com/gurst6/projeto-integrador-gantt)**
![alt text](images/api2.png)
O projeto em parceria com a empresa Necto tinha como problema a necessidade de organização dos diversos projetos realizados pela empresa. O objetivo do projeto era de desenvolver um software de visão estratégica de projetos e tarefas para o nosso cliente, onde ele poderia ter visão do cronograma dos seus projetos e as tarefas de cada projeto. Com isso, foi desenvolvido um projeto em Java com a utilização da biblioteca DHTMLX para a exibição de um diagrama de Gantt com as tarefas e projetos da empresa e seus cronogramas. Esse software contém um sitema de login, cadastro de projetos e tarefas, as quais são atreladas a um determinado projeto.

#### Tecnologias Utilizadas
* Java 8 - Back end
* Javascript - Front end
* Framework Spring-web - Estrutura MVC do projeto
* MySQL - Banco de Dados relacional
* DHTMLX - Interface do gráfico gantt
* Principais Bibliotecas:  
    * **DHTMLX - Exibição do diagrama de Gantt (Javascript).
    * **Hibernate - ORM (Java)

#### Contribuições Pessoais
Nesse projeto fui responsável por criar o back end do projeto e sua comunicação com o diagrama de Gantt. O back end consistia em um CRUD de projetos, colaboradores e tarefas, componentes necessários para popular o diagrama de Gantt utilizado para a organização dos projetos da empresa, assim como a comunicação com a biblioteca para criação de diagramas utilizada, o DHTMLX. 

#### Hard Skills Efetivamente Desenvolvidas
* Java: Back end.
* Spring: Framework para desenvolvimento utilizado no projeto.
* Mysql: Banco de dados relacional utilizado.
* DHTMLX: Utilização da biblioteca de exibição de gráficos e diagramas.

#### Soft Skills Efetivamente Desenvolvidas
* Proatividade: Estávamos em menor número do que o recomendado para esse trabalho, então foi necessário esforço e dedicação dos membros do grupo.
* Comunicação: Nesse semestre tivemos que iniciar o ensino emergêncial a distancia por conta da pandemia, portanto foi essencial manter uma comunicação mais eficaz por conta de não ter como se encontrar pessoalmente como anteriormente na faculdade.
* Organização: A organização foi melhorada ao longo do projeto, já que éramos poucos, foi necessário que os integrantes atuassem em diversos papeis.

### Em 2020-2 - Score wizard - **[(Gitlab do projeto)](https://gitlab.com/bielqcsjc/pi-scorewizard)**
![image](images/api3.png)
O SPC, em parceria com os alunos do 3º semestre do curso de Banco de Dados da Fatec de SJC, necessitava de um software para obter uma análise estatística da evolução do consumidor em relação ao histórico de pagamentos e da evolução da nota de score. Esse software seria usado por qualquer pessoa física ou jurídica, que possua cadastro positivo e que, ao utilizá-lo, espera melhorar seu score de uma forma melhor do que as alternativas existentes no mercado. Era esperado também, obter dados de sua evolução financeira, a fim de contribuir para uma melhora do score do utilizador, e, possivelmente, efetuar novas solicitações de crédito para as instituições financeiras. Com isso, foi desenvolvido uma API Rest em Java. Esse software contém as seguintes funcionalidades:
* Cadastro de usuário e Login: O usuário, para utilizar as ferramentas, precisa criar um conta baseado nas informações solicitados (Nome, CPF, data de nascimento e etc.). Após o cadastro efetuado, o usuário consegue logar na ferramenta para usufruir das demais funcionalidades;
* Análise automática de possíveis motivos de baixo score: O usuário consegue acessar a aplicação e encontrar quais as dívidas e faturas estão diminuindo a sua numeração de score;
* Análise do que seria necessário realizar para melhorar o score: O usuário encontrará na aplicação quais ações podem ajudá-lo a melhorar a pontuação do score;
* Gamificação: Implementamos um estilo de gamificação na aplicação, onde o usuário ganha pontos e avança de nível quando quita alguma conta. Esse nível pode ser utilizado para mostrar se o usuário tem um bom histórico de pagamentos.

#### Tecnologias Utilizadas
* Java 8 - Linguagem de programação
* Gradle - Framework de automação de compiçação
* Javascript - Linguagem para o front-end
* Framework Spring-web - Estrutura MVC do projeto
* MySQL - Banco de Dados relacional
* Principais Bibliotecas Java:
    * Hibernate - ORM para mapeamento objeto-relacional

#### Contribuições Pessoais
No projeto, colaborei com a organização das ideias entre as sprints e no desenvolvimento do back-end. O projeto necessitava de ideias atrativas para os usuários, então foi feito um brainstorming para reunirmos as melhores possibilidades. O back-end consistia em ter registrado diversos dados recebidos via .csv, que tiveram o conteúdo armazenado no banco de dados, além da consulta para o cálculo do score.

#### Hard Skills Efetivamente Desenvolvidas
* Java: Utilização da linguagem.
* Gradle - Gerenciador de dependências 
* Spring: Framework para desenvolvimento utilizado no projeto.
* Mysql: Banco de dados relacional utilizado.

#### Soft Skills Efetivamente Desenvolvidas
* Dinamismo: Estávamos em menor número do que o recomendado para esse trabalho, então necessitou que cada um de nós trabalhasse um pouco mais pra compensar.
* Comunicação: Nesse semestre tivemos que iniciar o ensino emergêncial a distancia por conta da pandemia, por tanto foi essencial manter uma comunicação mais eficaz por conta de não ter como se encontrar pessoalmente como anteriormente na faculdade.
* Organização: Por estar em casa, acabamos organizando mal o nosso tempo, por isso foi preciso aprender como se autogerenciar melhor.

### Em 2021-1 - Gerenciador de currículos - **[Gitlab do projeto](https://gitlab.com/gurst6/projeto-integrador-pythaon)**
![image](images/api4.png)

O problema apresentado era a necessidade de um bom desempenho em um gerenciador de currículos que contém diversos candidatos e vagas, além de ter uma inteligência que ajuda no cruzamento de vagas e currículos. Diversos novos candidatos e vagas eram inseridos todos os dias e era necessário que esses postos fossem preenchidos das maneiras mais eficiente possíveis. A inteligência era requisitada nesse momento, onde os requisitos para as vagas, como salário, qualificações, experiência profissional, VT0 (abstenção de pagamento de vale transporte quando a distância percorrida pelo trabalhador até o trabalho é menor do que 1km), deveriam ser analisados de forma com que os possíveis candidatos estivessem o mais próximo possível dos requisitos demandados para a vaga. A solução proposta foi uma api que gerenciasse os currículos e vagas, utilizando de ferramentas do MongoDB para otimizar buscas e cruzamentos de postos de trabalho, além das queries geoespaciais para buscas por VT0.

#### Tecnologias Utilizadas
* Python 3.8 - Linguagem principal
* FrameWork Django 3 - Serviços
* MongoDB - Banco de Dados 
    * **Pymongo** - interação com nosso Banco de Dados;

#### Contribuições Pessoais
Nesse projeto, fui responsável por criar os endpoints dos serviços responsáveis por cadastros de vagas e currículos e na criação das queries utilizadas no banco de dados (MongoDB). Os endpoints foram criados para cadastrar e manter os currículos e vagas da aplicação do cliente em um banco de dados NoSql, já que seu formato é volátil assim como as duas estruturas. As queries feitas no MongoDB envolvem encontrar currículos para determinada vagas de forma automática (quando uma nova vaga é inserida) ou parametrizada, onde o cliente escolhe os filtros para encontrar possíveis candidatos. Um dos filtros possíveis é o do VT0, que utiliza das queries de geolocalização do MongoDB para realizar, de forma performática, buscas baseadas em distância entre pontos geográficos.

#### Hard Skills Efetivamente Desenvolvidas
* Python: Aplicando os conhecimentos na construição dos serviços do projeto.
* Django: Utilizando o framework para construção do back-end do projeto.
* MongoDB: Mais conhecimento aplicado na utilização do MongoDB em relação a queries complexas e utilização de queries geoespaciais. 

#### Soft Skills Efetivamente Desenvolvidas
* Contato com o cliente: Atuei como Product Owner no projeto, além de desenvolvedor, então mantive contato direto com o cliente, alinhando as soluções com os problemas.
* Apresentação: Apresentar o projeto para o cliente, mostrando melhoria em expor o melhor da entrega.
* Organização: Por estar em casa, acabamos organizando mal o nosso tempo, por isso foi preciso aprender como se autogerenciar.

### Em 2021-2 - Sistema analítico educacional - **[Gitlab do projeto](https://gitlab.com/bielqcsjc/pythaoff-ness)**
![image](images/api5.png#center)

Foi solicitado pelo cliente fazer melhorias e trabalhar a parte analítica de um projeto voltado ao ensino à distância, onde uma plataforma foi desenvolvida e consistia na centralização dos envolvidos, como alunos e professores, e dos conteúdos a serem ministrados. Nosso trabalho foi melhorar o banco de dados para suportar grandes processamentos de dados, criar serviços de logs e armazenamento de chats e exibir informações referentes à plataforma para que seja possível a análise e o monitoramento da mesma. As melhorias de desempenho foram feitas através do particionamento do banco de dados, que também foi versionado durante o desenvolvimento. Ainda sobre o banco de dados, utilizamos bases diferentes: uma que armazena os dados da aplicação, outra desnormalizada a fim de ser um Data Warehouse para disponibilizar os dados analíticos e uma base noSql com MongoDB para armazenamento de logs e chat. Os resultados foram disponibilizados para análise utilizando o Power BI, criando a interface exibida na imagem acima.

#### Tecnologias Utilizadas
* Sql Server
* Docker
* Pentaho
* MongoDB
* Power BI

#### Contribuições Pessoais
Nesse projeto, construí a modelagem OLTP e auxiliei na modelagem OLAP, do Data Warehouse, utilizando o SGBD Sql Server, além da estrutura utilizada nos logs e chats da aplicação. Ajudei também no particionamento e versionamento dos banco de dados utilizados na aplicação. Assim como no projeto anterior, fui o PO da equipe, tendo contato direto com o cliente para trazer o que seria mais valioso para o mesmo, organizando as entregas e validadando os requisitos levantados, além de apresentar os resultados ao fim de cada sprint. Participei também da construição dos relatórios solicitados pelo cliente através da ferramenta Power BI, atuando principalmente na construção das queries utilizadas para recuperar os dados.

#### Hard Skills Efetivamente Desenvolvidas
* Mysql Server: Utilizado para armazenar os dados da aplicação e do Data Warehouse.
* CI/CD Gitlab: Utilizado para a integração e deploy contínuos.
* Docker: Utilizado junto ao CI/CD do git lab.
* Pentaho: Utilizado no processo de ETL do Data Warehouse.
* Power BI: Utilizando para construir os relatórios.
* MongoDB: Utilizado para armazenar os chats e logs. 

#### Soft Skills Efetivamente Desenvolvidas
* Contato com o cliente: Atuei como Product Owner no projeto, além de desenvolvedor, então mantive contato direto com o cliente, alinhando as soluções com os problemas.
* Apresentação: Uma dos principais pontos para a entrega era mostrar ao cliente o valor dos relatórios e dos processos quanto as melhorias da aplicação e da análise possível dos resultados obtidos.
* Entrega de valor: Por ser uma sequência de um projeto já existente, tivemos que pensar além da aplicação que já existia para entregar o valor desejado pelo cliente.

### Em 2022-1 - Análise de nuvens em imagens de satélite - **[Gitlab do projeto](https://gitlab.com/bielqcsjc/pi-clouds)**
![image](images/api6.png)

O objetivo do projeto é utilizar técnicas de processamento de imagens aplicada ao percentual de cobertura de nuvens contidas em uma imagem de satélite. Foi utilizado um algoritmo KNN para classificar os pixels através de exemplos pré-definidos de nuvem e terreno, exibindo a relação de quantidade e percentual de cada tipo de exemplo presente na imagem, além do exemplo utilizado e a imagem já processada.

#### Tecnologias Utilizadas
* Python
* KNN
* Filtragem de pixels
* matplotlib
  
#### Contribuições Pessoais
Utilizando o algoritmo KNN, recolhi exemplos de nuvens e terreno para classificar os pixels da imagem de satélite. Baseado nas classificações escolhidas, o parâmetro de vizinhos mais próximo (K) foi definido com o valor 15, ou seja, serão levados em consideração quinze valores do exemplo para classificar o pixel atual da imagem. Analisei os resultados e calculei o percentual de pixels de cada tipo analisado: nuvem e terreno. Os valores obtidos foram comparados com os calculados pelo satélite LandSat da NASA, sendo os valores de nuvem obtidos de 29.66% contra 26.25% do LandSat. A imagem e os exemplos utilizados foram reduzidos para 10% do valor original, para ter um tempo de processamento menor.

#### Hard Skills Efetivamente Desenvolvidas
* Python.
* KNN: Algoritmo de k-vizinhos mais próximos.
* Filtragem de pixels: Categorizando cada pixel da imagem de acordo com os exemplos fornecidos.
* Matplotlib: Biblioteca criação de gráficos e visualizações de dados em geral.

#### Soft Skills Efetivamente Desenvolvidas
* Capacidade analítica: Análise de algoritmos.

## Contatos
* [GIT](https://gitlab.com/bielqcsjc)
* [LinkedIn](https://www.linkedin.com/in/gabriel-queiroz-8a665815b/)

